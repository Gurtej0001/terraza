//
//  HelperClass.swift
//  Terraza SanMiguel
//
//  Created by Rakesh gupta on 11/14/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class HelperClass {
    
     typealias ASCompletionBlock = (_ result: NSDictionary, _ error: Error?, _ success: Bool) -> Void
    
    class func addShadowToBtn(btn:UIButton, radius:CGFloat) -> Void {
        btn.layer.masksToBounds = false
       // btn.layer.borderColor = UIColor.groupTableViewBackground.cgColor
        btn.layer.shadowColor = themeColorLightGreen.cgColor
        btn.layer.shadowOpacity = 0.8
        btn.layer.shadowRadius = radius
        
        btn.layer.shadowOffset = CGSize.init(width: 0.0, height: 0.0)
    }
    
    class func addShadowToView(view:UIView, radius:CGFloat) -> Void {
        view.layer.masksToBounds = false
        //view.layer.borderColor = UIColor .groupTableViewBackground.cgColor
        view.layer.shadowColor = UIColor.darkGray.cgColor
        view.layer.shadowOpacity = 0.8
        view.layer.shadowRadius = radius
        
        view.layer.shadowOffset = CGSize.init(width: 0.0, height: 0.0)
    }
    class func getLength(mobileNumber: String) -> Int {
        
        var strMobile =  mobileNumber;
        strMobile = strMobile.replacingOccurrences(of: "(", with: "")
        strMobile = strMobile.replacingOccurrences(of: ")", with: "")
        strMobile = strMobile.replacingOccurrences(of: " ", with: "")
        strMobile = strMobile.replacingOccurrences(of: "-", with: "")
        strMobile = strMobile.replacingOccurrences(of: "+", with: "")
        
        //Calculate lenght of string
        let length = strMobile.count;
        
        return length
    }
    
    class func showPopupAlertController(sender : Any, message : String, title : String) -> Void{
        
        let alert = UIAlertController(title: title as String,
                                      message: message as String,
                                      preferredStyle: UIAlertController.Style.alert)
        
        
        let OKAction = UIAlertAction(title: "OK",
                                     style: .default, handler: nil)
        
        alert.addAction(OKAction)
        UIApplication.shared.keyWindow?.rootViewController!.present(alert, animated: true,completion: nil)
        
        
    }

    
    //MARK:- Api methods
     class func requestForAllApiyWithBody(serverUrl urlString : String, completionHandler : @escaping ASCompletionBlock) -> Void{
                
            let headers = [
                        "Content-Type": "application/json",
                        "X-Requested-With": "XMLHttpRequest",
                        "cache-control": "no-cache",
                        ]

            let methodTypes: HTTPMethod = .get
         
            let param = ["" : ""]
           
            Alamofire.request(urlString, method: methodTypes, parameters: param,encoding: JSONEncoding.default, headers: headers).responseJSON {
                    response in
                    switch response.result {
                        case .success:
                        print(response)
                            if response.data != nil {
                                do {
                                    let json = try JSONSerialization.jsonObject(with: response.data!, options: [])
                                    let results = try? JSONSerialization.jsonObject(with: response.data!, options: [])
                                    let jsonData: Data? = try? JSONSerialization.data(withJSONObject: results! , options: .prettyPrinted)
                                    let myString = String(data: jsonData!, encoding: String.Encoding.utf8)
                                       print("Result: \(myString ?? "")")
                                       
                                    completionHandler(json  as! Dictionary<String, Any> as NSDictionary, nil, true)
                                       return
                                }catch {
                                       print(error.localizedDescription)
                                       completionHandler([:], error, false)
                                       return;
                                }
                            }
                            break
                        case .failure(let error):
                            print(error)
                            completionHandler([:], error, false)
                            return;
                    }
                }
        }

    
    
    class func requestForAllApiyWithBody( param : [String : Any],serverUrl urlString : String,andHeader isHeader : Bool , completionHandler : @escaping ASCompletionBlock) -> Void {
                   
                   
                   let headers = [
                       "Content-Type": "application/json",
                       "X-Requested-With": "XMLHttpRequest",
                       "cache-control": "no-cache",
                  
                   ]

        let methodTypes: HTTPMethod = .post
           
        Alamofire.request(urlString, method: methodTypes, parameters: param,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
            switch response.result {
            case .success:
                print(response)
                if response.data != nil {
                    do {
                        let json = try JSONSerialization.jsonObject(with:response.data!, options: [])
                        let results = try? JSONSerialization.jsonObject(with: response.data!, options: [])
                        let jsonData: Data? = try? JSONSerialization.data(withJSONObject: results! , options: .prettyPrinted)
                        let myString = String(data: jsonData!, encoding: String.Encoding.utf8)
                        print("Result: \(myString ?? "")")
                        
                        completionHandler(json  as! Dictionary<String, Any> as NSDictionary, nil, true)
                        return
                    }catch {
                        print(error.localizedDescription)
                        completionHandler([:], error, false)
                        return;
                    }
                }
                break
            case .failure(let error):
                print(error)
                completionHandler([:], error, false)
                return;
            }
        }
    }
    
}


