//
//  StringDefine.swift
//  Terraza SanMiguel
//
//  Created by Rakesh gupta on 11/14/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import Foundation
import UIKit

let  ACCEPTABLE_MOBILE_NUMBER                = "0123456789"
let themeColorLightGreen : UIColor! = UIColor().HexToColor(hexString: "#A6B490", alpha: 1.0)
let themeColorDarkGreen : UIColor! = UIColor().HexToColor(hexString: "#3C5142", alpha: 1.0)

var baseUrl = "http://192.168.1.17/mh-restaurant/api/martinez-restaurant-customer.php/"

var Url_Login               = baseUrl + "customerLogin"
var Url_ForgotPassword      = baseUrl + "forgotPassword"
var Url_VerifyOTP           = baseUrl + "verifyOtp"
var Url_ResendOTP           = baseUrl + "resendOtp"
var Url_UpdateProfile       = baseUrl + "updateCustomerProfile"
var Url_ChangePassword      = baseUrl + "changePassword"
var Url_Logout              = baseUrl + "logout"
var Url_OfferList           = baseUrl + "getOffers"
var Url_OrderList           = baseUrl + "getYourOrders"
var Url_FeedBack            = baseUrl + "customerFeedback"



var kCustomerID : String     = "customerID"
var kCustomerInfo : String   = "customerInfo"
var kAlertTitle: String      = "Terraza SanMiguel"
