//
//  ModelClass.swift
//  Terraza SanMiguel
//
//  Created by Rakesh Gupta on 11/29/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import Foundation

class UserDHolder: NSObject{
    
    private static var singltonVar:UserDHolder?
    @objc var customerId     = ""
    @objc var customerName   = ""
    @objc var doa            = ""
    @objc var dob            = ""
    @objc var email          = ""
    @objc var gender         = ""
    @objc var mobileNo       = ""
    @objc var profilePhoto   = ""
   
    class var sharedInstance : UserDHolder
    {
        guard let uwshared  = singltonVar else {
            singltonVar = UserDHolder()
            return singltonVar!
        }
        return uwshared
        
    }
    
    class func destroy() {
        
        singltonVar = nil
        
    }
    
}

class OrderListData : NSObject{
    
    @objc var orderId              = NSInteger()
    @objc var tableNo              = NSInteger()
    @objc var orderTime            = ""
    @objc var orderNo              = NSInteger()
    @objc var orderAmount          = NSInteger()
    @objc var discountType         = ""
    @objc var discountAmount       = NSInteger()
    @objc var netAmount            = NSInteger()
    @objc var enterAmount          = NSInteger()
    @objc var payBy                = ""
    @objc var orderStatus          = ""
    @objc var isPaid               = ""
    @objc var isCanceled           = ""
    @objc var orderItems           = [OrderSummaryData]()
    @objc var customDescription    = ""
    
    init(data: Dictionary<String,Any>) {
          super.init()
          
          for (key,value) in data {
              if let v = value as? Array<Dictionary<String,Any>> {
                  var arr = [OrderSummaryData]()
                  for dataModal in v {
                    let modal = OrderSummaryData.init(data: dataModal)
                     arr.append(modal)
                  }
                  self.setValue(arr, forKey: key)
                  
              }else{
                  self.setValue(value, forKey: key)
              }
              
          }
          
      }
      override init() {
          super.init()
      }
      
}


class OrderSummaryData : NSObject{
    
    @objc var itemName              = ""
    @objc var quantity              = NSInteger()
    @objc var customDescription     = ""
    @objc var isAdditional          = ""
    @objc var itemPrice             = NSInteger()
    @objc var itemTotal             = NSInteger()
    @objc var itemPhoto             = ""
  
    init(data:Dictionary<String,Any>) {
          super.init()
          for (key,value) in data {
              self.setValue(value, forKey: key)
          }
      }
}

class OfferListData : NSObject{
    
    @objc var couponId                = ""
    @objc var couponCode              = ""
    @objc var couponTitle             = ""
    @objc var couponDescription       = ""
   
  
    init(data:Dictionary<String,Any>) {
          super.init()
          for (key,value) in data {
              self.setValue(value, forKey: key)
          }
      }
}
