//
//  OrderCell.swift
//  Terraza SanMiguel
//
//  Created by Rakesh Gupta on 11/26/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {

    @IBOutlet weak var lblOrderStatus: UILabel!
    @IBOutlet weak var lblPaymentType: UILabel!
    @IBOutlet weak var lblAmountCFA: UILabel!
    @IBOutlet weak var lblOrderID: UILabel!
    @IBOutlet weak var lblOrderDate: UILabel!
    @IBOutlet weak var btnWriteReview: UIButton!
    @IBOutlet weak var viewMain: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        HelperClass.addShadowToView(view: viewMain, radius: 1)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    

    
}
