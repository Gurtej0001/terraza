//
//  OrderVC.swift
//  Terraza SanMiguel
//
//  Created by Rakesh Gupta on 11/26/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import UIKit
import SVProgressHUD

class OrderVC: BaseViewController,UITableViewDelegate,UITableViewDataSource{
  
    @IBOutlet weak var tblOrderList: UITableView!
    var arrOrderList = [OrderListData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UserDefaults.standard.string(forKey: "SelectedLanguage") == "en"{
            self.setTitleOnNavigationBar(name: "Orders")
        }else{
            self.setTitleOnNavigationBar(name: "Pedidos")
        }
        
        getOrderListApi()
    }
    
    //MARK:- TableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOrderList.count
      }
      
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblOrderList.dequeueReusableCell(withIdentifier: "OrderCell") as! OrderCell
        let orderListData = arrOrderList[indexPath.row]
        cell.lblOrderID.text = "\(orderListData.orderNo)"
        
        let orderDate : Date = NSDate(timeIntervalSince1970:  (orderListData.orderTime as! NSString).doubleValue) as Date
              let date1Formatter = DateFormatter()
              date1Formatter.dateFormat = "dd-MM-yyyy HH:mm" //Specify your format that you want
              let strOrderDate = date1Formatter.string(from: orderDate)
             
        cell.lblOrderDate.text = strOrderDate
        let myIntString = orderListData.orderAmount.formattedWithSeparator
        cell.lblAmountCFA.text = myIntString + " CFA"
        if orderListData.orderStatus == "Completed"{
            cell.lblOrderStatus.text = orderListData.orderStatus
            cell.lblOrderStatus.textColor = themeColorLightGreen
        }else{
            cell.lblOrderStatus.text = orderListData.orderStatus
            cell.lblOrderStatus.textColor = UIColor.red
        }
        
        cell.lblPaymentType.text = orderListData.payBy
        cell.btnWriteReview.addTarget(self, action: #selector(btnWriteReviewAction), for: .touchUpInside)
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let vc = self.storyboard?.instantiateViewController(identifier: "OrderSummary") as! OrderSummary
        vc.arrOrderSummary = [arrOrderList[indexPath.row]]
        self.navigationController?.pushViewController(vc, animated: true)
        self.tblOrderList.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
      
    
    @IBAction func btnWriteReviewAction(_ sender: UIButton) {
            
        let orderID = arrOrderList[sender.tag].orderNo
           let vc = self.storyboard?.instantiateViewController(identifier: "FeedBackVC") as! FeedBackVC
        vc.strOrderID = "\(orderID)"
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //MARK:- Api functions
    func getOrderListApi(){
        
        SVProgressHUD.show()
        let strCustomerID = UserDefaults.standard.string(forKey: kCustomerID)
        print(strCustomerID!)
        let perameter: [String: Any] = ["customerId": strCustomerID!]
        HelperClass.requestForAllApiyWithBody(param: perameter, serverUrl: Url_OrderList, andHeader: true){ (result:NSDictionary , error:Error? , success:Bool ) in
            if success {
                if (result.value(forKey: "errorCode")as! NSInteger == 0){
                    
                    self.arrOrderList.removeAll()
                    if let arrTemp = result["orderList"] as? NSArray {
                                          
                        for dict in arrTemp {
                            let data = OrderListData.init(data: dict as! Dictionary<String, Any>)
                            self.arrOrderList.append(data)
                        }
                    }
                    self.tblOrderList.reloadData()
                   
                }else{
                    
                     HelperClass.showPopupAlertController(sender: self, message: result.value(forKey: "message") as! String, title: kAlertTitle)
                }
                    
            }else if error != nil {
                HelperClass.showPopupAlertController(sender: self, message: (error?.localizedDescription)!, title: kAlertTitle);
                
            }else {
                HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss();
        }
    
    }
}



//    func getOrderListApi(){
//        SVProgressHUD.show()
//        let urlString = Url_OrderList
//        let strCustomerID = UserDefaults.standard.string(forKey: kCustomerID)
//        print(strCustomerID!)
//        let perameter: [String: Any] = ["customerId": strCustomerID!]
//
//        Alamofire.request(urlString, method: .post, parameters: perameter,encoding: JSONEncoding.default, headers: nil).responseJSON {
//            response in
//            print(response.request!)
//            switch response.result {
//            case .success:
//                print(response.result.value!)
//
//                let dict = response.result.value! as! NSDictionary
//
//                let errorCode = dict["errorCode"] as! Int
//                if errorCode == 0{
//                    self.arrOrderList.removeAll()
//                    if let arrTemp = dict["orderList"] as? NSArray {
//
//                        for dict in arrTemp {
//                            let data = OrderListData.init(data: dict as! Dictionary<String, Any>)
//                            self.arrOrderList.append(data)
//                        }
//                    }
//                    self.tblOrderList.reloadData()
//
//
//                }else{
//                    let message = dict["message"] as! String
//                    self.addAlertBox(str: message)
//                }
//
//            case .failure(let error):
//                 self.addAlertBox(str: "Something went wrong.")
//                print(error)
//            }
//            SVProgressHUD.dismiss()
//        }
//    }
