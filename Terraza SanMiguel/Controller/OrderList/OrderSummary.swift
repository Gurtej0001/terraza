//
//  OrderSummary.swift
//  Terraza SanMiguel
//
//  Created by Rakesh Gupta on 11/26/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import UIKit
import SVProgressHUD

class OrderSummary: BaseViewController,UITableViewDataSource,UITableViewDelegate {
  
    @IBOutlet weak var tblOrderSummary: UITableView!
    @IBOutlet weak var lblItemTotal: UILabel!
    @IBOutlet weak var lblDiscount: UILabel!
    @IBOutlet weak var lblNetAmount: UILabel!
    @IBOutlet weak var lblEnterAmount: UILabel!
    @IBOutlet weak var lblPayby: UILabel!
    @IBOutlet weak var lblChangeAmount: UILabel!
    
    var arrOrderSummary = [OrderListData]()
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let data = arrOrderSummary[0]
        let orderAmountString = data.orderAmount.formattedWithSeparator
        lblItemTotal.text = orderAmountString
        let discountAmountString = data.discountAmount.formattedWithSeparator
        lblDiscount.text = discountAmountString
        let netAmountString = data.netAmount.formattedWithSeparator
        lblNetAmount.text = netAmountString
        let enterAmountString = data.enterAmount.formattedWithSeparator
        lblEnterAmount.text = enterAmountString
          //     vc.lblChangeAmount.text = DataOrderList.
        lblPayby.text = data.payBy
        let changeAmount = data.enterAmount - data.netAmount
        lblChangeAmount.text =  "\(changeAmount)"
        
        self.setTitleOnNavigationBar(name: "Order #" + " \(data.orderNo)")
        
        
    }
    
    //MARK:- TableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOrderSummary[0].orderItems.count
      }
      
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblOrderSummary.dequeueReusableCell(withIdentifier: "OrderSummaryCell") as! OrderSummaryCell
        let dataOrderSummary = arrOrderSummary[0].orderItems[indexPath.row]
        cell.lblTitle.text = dataOrderSummary.itemName
        let itemPriceString = dataOrderSummary.itemPrice.formattedWithSeparator
        cell.lblItemAmount.text = itemPriceString
        cell.lblNoCountity.text = "\(dataOrderSummary.quantity)"
        let itemTotalString = dataOrderSummary.itemTotal.formattedWithSeparator
        cell.lblTotalAmount.text = itemTotalString
        
        return cell
    }
      
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           self.tblOrderSummary.deselectRow(at: indexPath, animated: true)
    }
    

}
