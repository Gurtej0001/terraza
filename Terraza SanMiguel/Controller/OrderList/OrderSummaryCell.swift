//
//  OrderSummaryCell.swift
//  Terraza SanMiguel
//
//  Created by Rakesh Gupta on 11/26/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import UIKit

class OrderSummaryCell: UITableViewCell {

    
    @IBOutlet weak var viewItemCountity: UIView!
    
    @IBOutlet weak var lblNoCountity: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblItemAmount: UILabel!
    @IBOutlet weak var lblTotalAmount: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
        viewItemCountity.layer.borderWidth = 1
        viewItemCountity.layer.borderColor = themeColorDarkGreen.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
