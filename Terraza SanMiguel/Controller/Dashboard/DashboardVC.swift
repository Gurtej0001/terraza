//
//  DashboardVC.swift
//  Terraza SanMiguel
//
//  Created by Rakesh Gupta on 11/26/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import UIKit
import SDWebImage

class DashboardVC: BaseViewController,UITableViewDelegate,UITableViewDataSource{
  
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobNo: UILabel!
    
    var arrData = ["Offers","Orders"]
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if UserDefaults.standard.string(forKey: "SelectedLanguage") == "en"{
            self.setMenuBarOnHeader(named : "Dashboard")
        }else{
             self.setMenuBarOnHeader(named : "Tablero")
        }
       
        lblName.text = self.userDetails.customerName
        lblMobNo.text = self.userDetails.mobileNo
        if self.userDetails.profilePhoto != ""{
            let imageURL = URL(string: self.userDetails.profilePhoto)!
            //  imageView.sd_setImage(with: imageURL)
            imgProfile.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "client.png"),options: SDWebImageOptions(rawValue: 0), completed: nil)
        }
    }
    
    //MARK:- TableView Delegats
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblView.dequeueReusableCell(withIdentifier: "DashboardCell") as! DashboardCell
        cell.lblTitle.text = arrData[indexPath.row]
        return cell
      }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 1{
            let vc = self.storyboard?.instantiateViewController(identifier: "OrderVC") as! OrderVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            let vc = self.storyboard?.instantiateViewController(identifier: "OfferListVC") as! OfferListVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        self.tblView.deselectRow(at: indexPath, animated: true)
    }
    
    
    //MARK:- Buttons Action
    
    @IBAction func btnProfileAction(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(identifier: "ProfileViewController") as! ProfileViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    //MARK:- NavigationController Function.
    func setMenuBarOnHeader(named : String){
        
        self.navigationController?.isNavigationBarHidden = false
           
        let font: UIFont? = UIFont.init(name: "quarca", size: CGFloat(22))!//UIFont.boldSystemFont(ofSize: CGFloat(fontSize))
        let attributes = [NSAttributedString.Key.font: font]
               
        let btnItemText = UIBarButtonItem(title: named, style: .plain, target: self, action: nil)
            btnItemText.tintColor = UIColor.white
            btnItemText.setTitleTextAttributes(attributes as [NSAttributedString.Key : Any], for: .normal)
        let items1 = [btnItemText]
            navigationItem.leftBarButtonItems = items1
        
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil);
        let btnMenu = UIButton.init(type: UIButton.ButtonType.custom);
        btnMenu.bounds = CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: CGSize.init(width: 20, height: 20));
        btnMenu.setImage(UIImage.init(named: "menu.png"), for: UIControl.State.normal);
        btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)) , for: UIControl.Event.touchUpInside);
        let btnBarItem1 = UIBarButtonItem.init(customView: btnMenu);
        self.navigationItem.rightBarButtonItem = btnBarItem1;
    }

}
