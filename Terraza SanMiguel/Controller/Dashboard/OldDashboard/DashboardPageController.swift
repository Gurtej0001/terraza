//
//  DashboardPageController.swift
//  Terraza SanMiguel
//
//  Created by Rakesh gupta on 11/13/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class DashboardPageController: ButtonBarPagerTabStripViewController {

    
    @IBOutlet weak var tabBarView: ButtonBarView!
    var  borderview  = UIView()
    var index = 0
    let arrHeaderList = ["Offers","Orders"]
    var arrViewControllers = NSMutableArray()
    
    //buttonBarView.selectedBar.backgroundColor = UIColor().HexToColor(hexString:"#A6B490")
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //         HelperClass.addShadowToView(view: self.viewLatestPoll, radius: 3.0);
        
        buttonBarView.selectedBar.backgroundColor = UIColor().HexToColor(hexString:"#A6B490")
        buttonBarView.backgroundColor = UIColor.clear
        
        // change selected bar color
        settings.style.buttonBarBackgroundColor = .clear
        settings.style.buttonBarItemBackgroundColor = .clear
        settings.style.selectedBarBackgroundColor = .clear
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            //settings.style.buttonBarItemFont = UIFont.init(name: "OpenSans", size: 16)!
        }
        settings.style.buttonBarItemFont = UIFont.init(name: "quarca", size: 16)!
        settings.style.selectedBarHeight = 0.0//3.0
        settings.style.buttonBarMinimumLineSpacing = 15
        settings.style.buttonBarItemTitleColor = UIColor().HexToColor(hexString:"#A6B490")
        settings.style.buttonBarItemsShouldFillAvailableWidth = true
        settings.style.buttonBarLeftContentInset = 15
        settings.style.buttonBarRightContentInset = 15
        
        changeCurrentIndexProgressive = { [weak self] (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
//            if UIDevice.current.userInterfaceIdiom == .pad {
//                oldCell?.label.font = UIFont.init(name: "OpenSans", size: 16)!
//                newCell?.label.font = UIFont.init(name: "OpenSans-Bold", size: 16)!
//            }else {
//                oldCell?.label.font = UIFont.init(name: "OpenSans", size: 14)!
//                newCell?.label.font = UIFont.init(name: "OpenSans-Bold", size: 14)!
//            }
        }
//        if arrCategoryDetail.count > 0 {
//            let dataHome = arrCategoryDetail[0]
//            self.navigationController?.navigationBar.barTintColor =  UIColor().HexToColor(hexString: dataHome.catColor, alpha: 1.0)
//
//            self.buttonBarView.backgroundColor =  UIColor().HexToColor(hexString: dataHome.catColor, alpha: 1.0)
//        }else {
//            let data = categoryData.sub_cat_arr[index]
//            self.navigationController?.navigationBar.barTintColor =  UIColor().HexToColor(hexString: data.subCatColor, alpha: 1.0)
//            self.buttonBarView.backgroundColor =  UIColor().HexToColor(hexString: data.subCatColor, alpha: 1.0)
//        }
        self.perform(#selector(setDataAfterDelay), with: self, afterDelay: 0.5)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        buttonBarView.bounces = false
        borderview.removeFromSuperview()
        borderview = UIView.init(frame: CGRect.init(x: 0, y: 49, width: buttonBarView.contentSize.width, height: 1.0))
        borderview.backgroundColor = UIColor.black
        buttonBarView.addSubview(borderview)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @objc func setDataAfterDelay() {
        self.moveToViewController(at: index)
    }
    
    
    override public func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        for index in 0..<arrHeaderList.count{
            let data = arrHeaderList[index]
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            vc.itemInfo = IndicatorInfo.init(title: data.uppercased())
            
            
            arrViewControllers.add(vc)
        }
        
        return arrViewControllers as! [UIViewController]
        
    }
    
    
    
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool) {
        super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)
        
       // self.navigationController?.navigationBar.barTintColor =  themeColor
        //self.buttonBarView.backgroundColor = themeColor
    }
}
