//
//  ContainerViewController.swift
//  Terraza SanMiguel
//
//  Created by Rakesh gupta on 11/13/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import UIKit
import SDWebImage

class ContainerViewController: BaseViewController {
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblMobileNo: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setMenuBarOnHeader()

        let controller = self.storyboard?.instantiateViewController(withIdentifier: "DashboardPageController") as! DashboardPageController

        
        DispatchQueue.main.async(execute: {() -> Void in
            self.addChild(controller)
            
            controller.view.translatesAutoresizingMaskIntoConstraints = false
            self.containerView.addSubview((controller.view)!)
            NSLayoutConstraint.activate([
                controller.view.leadingAnchor.constraint(equalTo: self.containerView.leadingAnchor),
                controller.view.trailingAnchor.constraint(equalTo: self.containerView.trailingAnchor),
                controller.view.topAnchor.constraint(equalTo: self.containerView.topAnchor),
                controller.view.bottomAnchor.constraint(equalTo: self.containerView.bottomAnchor)
                ])
            
            
            controller.didMove(toParent: self)
        })
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        lblName.text = self.userDetails.customerName
        lblMobileNo.text = self.userDetails.mobileNo
        if self.userDetails.profilePhoto != ""{
             let imageURL = URL(string: self.userDetails.profilePhoto)!
            //  imageView.sd_setImage(with: imageURL)
            imgProfile.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "client.png"),options: SDWebImageOptions(rawValue: 0), completed: nil)
        }
       
       
    }
    

    func setMenuBarOnHeader(){
        if UserDefaults.standard.string(forKey: "SelectedLanguage") == "es"{
            self.navigationItem.title = "Tablero"
        }else{
            self.navigationItem.title = "Dashboard"
        }
       
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil);
        let btnMenu = UIButton.init(type: UIButton.ButtonType.custom);
        btnMenu.bounds = CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: CGSize.init(width: 22, height: 22));
        btnMenu.setImage(UIImage.init(named: "menu.png"), for: UIControl.State.normal);
        btnMenu.addTarget(self, action: #selector(self.btnMenuAction(_:)) , for: UIControl.Event.touchUpInside);
        let btnBarItem1 = UIBarButtonItem.init(customView: btnMenu);
        self.navigationItem.rightBarButtonItem = btnBarItem1;
    }

}
