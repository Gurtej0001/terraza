//
//  DashboardViewController.swift
//  Terraza SanMiguel
//
//  Created by Rakesh gupta on 11/13/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class DashboardViewController: BaseViewController , IndicatorInfoProvider {

    var itemInfo: IndicatorInfo = "View"
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    // MARK: - IndicatorInfoProvider
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

}
