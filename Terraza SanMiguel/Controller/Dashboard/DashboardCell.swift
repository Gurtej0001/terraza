//
//  DashboardCell.swift
//  Terraza SanMiguel
//
//  Created by Rakesh Gupta on 11/26/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import UIKit

class DashboardCell: UITableViewCell {
    
    
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    
    @IBOutlet weak var viewMain: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewMain.layer.borderWidth = 1
        viewMain.layer.borderColor = themeColorLightGreen.cgColor
        HelperClass.addShadowToView(view: viewMain, radius: 2)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
