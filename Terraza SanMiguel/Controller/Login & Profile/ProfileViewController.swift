//
//  ProfileViewController.swift
//  Terraza SanMiguel
//
//  Created by Rakesh gupta on 11/14/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import SVProgressHUD
import Alamofire
import SDWebImage

class ProfileViewController: BaseViewController,UITextFieldDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate{

    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var txtFullName: ACFloatingTextfield!
    @IBOutlet weak var txtMobileNo: ACFloatingTextfield!
    @IBOutlet weak var txtEmailID: ACFloatingTextfield!
    @IBOutlet weak var txtDOB: ACFloatingTextfield!
    @IBOutlet weak var txtDOA: ACFloatingTextfield!
    @IBOutlet weak var txtGender: ACFloatingTextfield!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    
    var imagePicker = UIImagePickerController();
    var datePicker = UIDatePicker()
    var strTextType = String()
    var delegate: SenderViewControllerDelegate?  
    
    override func viewDidLoad() {
        super.viewDidLoad()

        HelperClass.addShadowToBtn(btn: btnSubmit, radius: 2)
        txtFullName.text = self.userDetails.customerName
        txtMobileNo.text = self.userDetails.mobileNo
        txtEmailID.text = self.userDetails.email
        txtGender.text = self.userDetails.gender
        
        if UserDefaults.standard.string(forKey: "SelectedLanguage") == "en"{
            self.setTitleOnNavigationBar(name: "Profile")
            if self.userDetails.gender == "m"{
                    txtGender.text = "Male"
            }else{
                    txtGender.text = "Female"
            }
        }else{
            self.setTitleOnNavigationBar(name: "Perfil")
            if self.userDetails.gender == "m"{
                txtGender.text = "Masculina"
            }else{
                txtGender.text = "Hembra"
            }
            
        }
        
     
        if self.userDetails.profilePhoto != ""{
            let imageURL = URL(string: self.userDetails.profilePhoto)!
            
            imgProfile.sd_setImage(with: imageURL, placeholderImage: UIImage(named: "client.png"),options: SDWebImageOptions(rawValue: 0), completed: nil)
        }
      
        
        
        let dobDate : Date = NSDate(timeIntervalSince1970:  (self.userDetails?.dob as! NSString).doubleValue) as Date
        let date1Formatter = DateFormatter()
        date1Formatter.dateFormat = "dd-MM-yyyy" //Specify your format that you want
        let strDOBDate = date1Formatter.string(from: dobDate)
        print(strDOBDate)
        txtDOB.text = strDOBDate
        
        let doaDate : Date = NSDate(timeIntervalSince1970:  (self.userDetails?.doa as! NSString).doubleValue) as Date
        let date2Formatter = DateFormatter()
        date2Formatter.dateFormat = "dd-MM-yyyy" //Specify your format that you want
        let strDOADate = date2Formatter.string(from: doaDate)
        print(strDOADate)
        txtDOA.text = strDOADate
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    NotificationCenter.default.addObserver(self, selector: #selector(getGenderValue), name: Notification.Name("strGenderType"), object: nil)
       
        
    }
    
    @objc func getGenderValue(noty : NSNotification){
        let strGender = noty.object as! String
        txtGender.text = strGender
        
    }
    
    //Date Picker Delegats
    func showDatePicker(){

        //Formate Date
        datePicker.datePickerMode = .date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        
      
        if strTextType == "DOB"{
            datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -100, to: Date())
            datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
            
            let strDob = dateFormatter.date(from: txtDOB.text!)
            datePicker.date = strDob!

        }else{
            
            datePicker.minimumDate = Calendar.current.date(byAdding: .year, value: -100, to: Date())
            datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: 0, to: Date())
            let strDob = dateFormatter.date(from: txtDOA.text!)
            datePicker.date = strDob!

        }
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        var strBtn1Title = String()
        var strBtn2Title = String()
        if UserDefaults.standard.string(forKey: "SelectedLanguage") == "en"{
            strBtn1Title = "Done"
            strBtn2Title = "Cancel"
        }else{
            strBtn1Title = "Hecho"
            strBtn2Title = "Cancelar"
        }
        let doneButton = UIBarButtonItem(title: strBtn1Title, style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: strBtn2Title, style: .plain, target: self, action: #selector(cancelDatePicker));

        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)

        
        if strTextType == "DOB"{
            txtDOB.inputAccessoryView = toolbar
            txtDOB.inputView = datePicker
        }else{
            txtDOA.inputAccessoryView = toolbar
            txtDOA.inputView = datePicker
        }

    }

    @objc func donedatePicker(){

        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy"
        
        if strTextType == "DOB"{
           txtDOB.text = formatter.string(from: datePicker.date)
        }else{
           txtDOA.text = formatter.string(from: datePicker.date)
        }
        self.view.endEditing(true)
    }

    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    
    //MARK:- TextField delegats
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtDOB{
            strTextType = "DOB"
             self.showDatePicker()
        }else if textField == txtDOA{
            strTextType = "DOA"
             self.showDatePicker()
        }else if textField == txtGender{
            textField.resignFirstResponder()
            let arrViews: Array? = Bundle.main.loadNibNamed("ViewSelectGender", owner: self, options: nil)
            
           let viewGenderPopup = (arrViews?[0] as? ViewSelectGender)!
           
        //    let viewGenderPopup = (UINib(nibName: "ViewSelectGender", bundle: nil).instantiate(withOwner: self, options: nil)[0] as? ViewSelectGender)!
            print(txtGender.text!)
            //viewGenderPopup.strGenderType = txtGender.text!
          //  self.delegate?.strMessage(str: txtGender.text!)
            viewGenderPopup.strMessage(str: txtGender.text!)
    //        viewGenderPopup.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height:  self.view.frame.size.height)
            viewGenderPopup.frame = UIApplication.shared.keyWindow!.frame
           // self.view.addSubview(viewGenderPopup)
            UIApplication.shared.keyWindow!.addSubview(viewGenderPopup)
        }
        
      
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:- image Picker functions
    
    func openCamera(){
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            self.present(imagePicker, animated: true, completion: nil)
        }else{
            // superclass.showPopupAlertController(sender: self, message: "camera not found", title: "Jrney");
            
        }
    }
    
    func openGallary(){
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK:- UIImagePickerControllerDelegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        
        let chosenImage = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
      //  imgProfile.contentMode = .scaleToFill
        imgProfile.image = chosenImage
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController){
        print("picker cancel.")
        dismiss(animated: true, completion: nil)
    }
    
    
    func GetFormatedDate(date_string:String,dateFormat:String)-> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = dateFormat
        
        let dateFromInputString = dateFormatter.date(from: date_string)
        dateFormatter.dateFormat = "dd-MM-yyyy" // Here you can use any dateformate for output date
        if(dateFromInputString != nil){
            return dateFormatter.string(from: dateFromInputString!)
        }
        else{
            debugPrint("could not convert date")
            return "N/A"
        }
    }
    
    func dateConvertIntoDDMMYYYY(strDate: String){
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "dd MM yyyy"
        
        if let date = dateFormatterGet.date(from: strDate) {
            print(dateFormatterPrint.string(from: date))
        } else {
            print("There was an error decoding the string")
        }
    }
    
   // "yyyy-MM-dd'T'HH:mm:ss"
    
    //MARK:- dd-mm-yyy into datestamp
    func convertDateFormatter(strDate: String) -> Any {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let dob = dateFormatter.date(from: strDate)

        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        let dateInterval = dob!.timeIntervalSince1970;
        print(dateInterval)
        
        return dateInterval
//        let strNewDate : Date = NSDate(timeIntervalSince1970:  (self.userDetails?.dob as! NSString).doubleValue) as Date
//        return "\(strNewDate)"
    }
    
     func isValidEmailAddress(emailAddressString: String) -> Bool {
        
        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
        
        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))
            
            if results.count == 0
            {
                returnValue = false
            }
            
        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }
        
        return  returnValue
    }
    
    //MARK:- Buttons Action
    
    @IBAction func btnEditProfileAction(_ sender: Any) {
        
        let alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openCamera()
        }
        let gallaryAction = UIAlertAction(title: "Gallary", style: UIAlertAction.Style.default)
        {
            UIAlertAction in
            self.openGallary()
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel)
        {
            UIAlertAction in
        }
        
        // Add the actions
        self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        self.present(alert, animated: true, completion: nil)
    }

    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        if txtFullName.text!.isEmpty{
           self.addAlertBox(str: "Please Enter Full Name.")
        }else if txtEmailID.text != ""{
            if isValidEmailAddress(emailAddressString: txtEmailID.text!) == false{
            self.addAlertBox(str: "Please Enter Valid Email.")
            }else{
                self.updateProfileWithFormdata()
            }
        }else{
             self.updateProfileWithFormdata()
        }
        
    }
    

    //MARK:- Api's Functions
//    func updateProfileApi(){
//        SVProgressHUD.show()
//
//        let strDob = self.convertDateFormatter(date: txtDOB.text!)
//        print(strDob)
//        let strDoa = self.convertDateFormatter(date: txtDOA.text!)
//        print(strDoa)
//
//        let urlString = Url_UpdateProfile
//        let perameter: [String: Any] = ["customerId": self.userDetails.customerId,"customerName": txtFullName.text!,"dob":strDob,"doa":strDoa,"email":txtEmailID.text!,"gender":self.userDetails.gender]
//
//        Alamofire.request(urlString, method: .post, parameters: perameter,encoding: JSONEncoding.default, headers: nil).responseJSON {
//            response in
//            print(response.request!)
//            switch response.result {
//            case .success:
//                print(response.result.value!)
//
//                let JSON = response.result.value! as! NSDictionary
//
//                let errorCode = JSON["errorCode"] as! Int
//                if errorCode == 0{
//                    let message = JSON["message"] as! String
//                    self.addAlertBox(str: message)
//
//                }else{
//                    let message = JSON["message"] as! String
//                    self.addAlertBox(str: message)
//                }
//
//            case .failure(let error):
//
//                print(error)
//            }
//            SVProgressHUD.dismiss()
//        }
//    }
    
    
    func updateProfileWithFormdata(){
    
        SVProgressHUD.show()
        let strDob = self.convertDateFormatter(strDate: txtDOB.text!)
        print(strDob)
        let strDoa = self.convertDateFormatter(strDate: txtDOA.text!)
        print(strDoa)
        var strGen = String()
       
        if txtGender.text == "Male" || txtGender.text == "Masculina"{
            strGen = "m"
        }else{
            strGen = "f"
        }
        let parameters = ["customerId": self.userDetails.customerId,"customerName": txtFullName.text!,"dob":"\(strDob)","doa":"\(strDoa)","email":txtEmailID.text!,"gender":strGen]
        
      //  let imgDData = UIImageJPEGRepresentation(imgProfile.image!, 0.3)
        let imageData = imgProfile.image!.jpegData(compressionQuality: 0.75)
        
        let urlString = Url_UpdateProfile
        let url = URL(string: urlString)!
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "POST"
        print(urlRequest)
       
        
        do {
            urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
        } catch {
            print(error)
        }
        
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        let imgData = imageData
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imgData!, withName: "profilePhoto",fileName: "file.jpg", mimeType: "profilePhoto/jpg")
            for (key, value) in parameters {
                //    multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
            } //Optional for extra parameters
        },
                         to: Url_UpdateProfile,
                         method:.post)
        { (result) in
            switch result {
            case .success(let upload, _, _):do {
                
                upload.uploadProgress(closure: { (progress) in
                    //      print("Upload Progress: \(progress.fractionCompleted)")
                })
                
                upload.responseJSON { response in
                    print(response.result.value!)
                    let dict = response.result.value! as! NSDictionary
                    
                    let errorCode = dict["errorCode"] as! Int
                    if errorCode == 0{
                        let message = dict["message"] as! String
                        self.addAlertBox(str: message)
                        
                        let dictCustomerInfo = dict["customerInfo"] as! NSDictionary
                     //   let dictCustomerInfo = arrCustomerInfo["] as! NSDictionary
                        
                        let userData = UserDHolder.sharedInstance
                        userData.setValuesForKeys(dictCustomerInfo as! [String : Any])
                        
                    }else{
                        let message = dict["message"] as! String
                        self.addAlertBox(str: message)
                    }
                    
                    SVProgressHUD.dismiss()
                }
                }
            case .failure(let encodingError):
                print(encodingError)
                self.addAlertBox(str: "Something went wrong.")
                SVProgressHUD.dismiss()
            }
        }
        
        
    }
    
}

//extension String {
//    func isValidEmail() -> Bool {
//        // here, `try!` will always succeed because the pattern is valid
//        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
//        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
//    }
//}
