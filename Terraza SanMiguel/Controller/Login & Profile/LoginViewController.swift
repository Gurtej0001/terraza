//
//  LoginViewController.swift
//  Terraza SanMiguel
//
//  Created by Rakesh gupta on 11/13/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import UIKit
import LRTextField
import ACFloatingTextfield_Swift

import SVProgressHUD

class LoginViewController: BaseViewController,UITextFieldDelegate {

   // @IBOutlet weak var txtMobile: LRTextField!
    @IBOutlet weak var txtMobile: ACFloatingTextfield!
    @IBOutlet weak var txtPassword: ACFloatingTextfield!
    
    @IBOutlet weak var btnEye: UIButton!
    
    @IBOutlet weak var viewMobileField: UIView!
    @IBOutlet weak var viewPasswordField: UIView!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    var tap = UITapGestureRecognizer()
    override func viewDidLoad() {
        super.viewDidLoad()

        viewMobileField.layer.borderWidth = 0.5
        viewPasswordField.layer.borderWidth = 0.5
        viewMobileField.layer.borderColor = UIColor.lightGray.cgColor
        viewPasswordField.layer.borderColor = UIColor.lightGray.cgColor
        
        btnSubmit.layer.borderWidth = 1
        btnSubmit.layer.borderColor = themeColorLightGreen.cgColor
        
        HelperClass.addShadowToBtn(btn: btnSubmit, radius: 2)
        

       
        // Do any additional setup after loading the view.
        tap = UITapGestureRecognizer(target: self, action: #selector(self.KeyPadTap))
        view.addGestureRecognizer(tap)
    
    }
    
    @objc func KeyPadTap() -> Void {
        self.view.endEditing(true)
    }
    
    //MARK: - Textfield Delegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
        tap = UITapGestureRecognizer(target: self, action: #selector(self.KeyPadTap))
        view.addGestureRecognizer(tap)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.view .removeGestureRecognizer(tap)
    }
    
    /*  return keyborad to the next textfield   */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        let tag: Int = textField.tag + 1
        let nextResponder: UIResponder? = textField.superview?.superview?.viewWithTag(tag)
        if (nextResponder is UITextField) {
            nextResponder?.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == txtMobile {
            let newLength: Int = (textField.text?.count)! + (string.count ) - range.length
            let csTxt = CharacterSet(charactersIn: ACCEPTABLE_MOBILE_NUMBER).inverted
            let filteredUsrTxt: String = string.components(separatedBy: csTxt).joined(separator: "")
            if (string == filteredUsrTxt) {
                if newLength > 20 {
                    return false
                }
            }
            else {
                return false
            }
            let length: Int = HelperClass.getLength(mobileNumber: textField.text!)
            //NSLog(@"Length  =  %d ",length);
            if length >= 13 {
                if range.length == 0 {
                    return false
                }
            }
        }
        return true
    }
    
    //MARK: - Button Action
    
    
    @IBAction func btnHideShowPasswordAction(_ sender: UIButton) {
        if txtPassword.text!.isEmpty{
            self.addAlertBox(str: "Empty Password")
        }else{
            if sender.tag == 0{
                     sender.tag = 1
                     self.txtPassword.isSecureTextEntry = false
                     self.btnEye.setImage(UIImage.init(named: "eye.png"), for: .normal)
                 }else{
                     sender.tag = 0
                     self.txtPassword.isSecureTextEntry = true
                     self.btnEye.setImage(UIImage.init(named: "1.png"), for: .normal)
                 }
        }
     
    }
    
    @IBAction func btnForgotPasswordAction(_ sender: Any) {
        if txtMobile.text!.isEmpty{
            self.addAlertBox(str: "Please Enter Mobile No.")
        }else{
            self.forgotPasswordApi()
        }
      
    }
    
    
    @IBAction func btnSubmitAction(_ sender: Any) {
  
        if txtMobile.text!.isEmpty{
             self.addAlertBox(str: "Please Enter Mobile No.")
        }else if txtPassword.text!.isEmpty{
             self.addAlertBox(str: "Please Enter The Password")
        }else{
             self.loginApi()
        }
       
    }
    
    
    //MARK:- Api's functions

    func forgotPasswordApi(){
        SVProgressHUD.show()
        let perameter: [String: Any] = ["mobileNo": self.txtMobile.text!]
        HelperClass.requestForAllApiyWithBody(param: perameter, serverUrl: Url_ForgotPassword, andHeader: true){ (result:NSDictionary , error:Error? , success:Bool ) in
            if success {
                if (result.value(forKey: "errorCode")as! NSInteger == 0){
                    let customerID = result["customerId"] as? NSString
                    UserDefaults.standard.set(customerID, forKey: kCustomerID)
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
                    vc.strCustomerID = customerID! as String
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }else{
                    
                     HelperClass.showPopupAlertController(sender: self, message: result.value(forKey: "message") as! String, title: kAlertTitle)
                }
                    
            }else if error != nil {
                HelperClass.showPopupAlertController(sender: self, message: (error?.localizedDescription)!, title: kAlertTitle);
                
            }else {
                HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss();
        }
    }
    
    
    func loginApi(){
        SVProgressHUD.show()
        let perameter: [String: Any] = ["mobileNo": self.txtMobile.text!,"password": self.txtPassword.text!]
        HelperClass.requestForAllApiyWithBody(param: perameter, serverUrl: Url_Login, andHeader: true){ (result:NSDictionary , error:Error? , success:Bool ) in
            if success {
                if (result.value(forKey: "errorCode")as! NSInteger == 0){
                    let dictCustomerInfo = result["customerInfo"] as! NSDictionary
                  //  let dictCustomerInfo = arrCustomerInfo[0] as! NSDictionary
                    UserDefaults.standard.set(dictCustomerInfo, forKey: kCustomerInfo)
                    
                    let strCustomerID = dictCustomerInfo["customerId"] as! String
                    UserDefaults.standard.set(strCustomerID, forKey: kCustomerID)
                    
                
                    let userData = UserDHolder.sharedInstance
                    userData.setValuesForKeys(dictCustomerInfo as! [String : Any])
                    
                    let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
                    let vc = storyboard.instantiateViewController(withIdentifier: "DashboardNavigationVC") as? UINavigationController
                    let appDl = UIApplication.shared.delegate as? AppDelegate
                    let previousRootViewController: UIViewController? = appDl?.window?.rootViewController
                    appDl?.window?.rootViewController = vc
                    
                    // Allow the view controller to be deallocated
                    previousRootViewController?.dismiss(animated: false) {
                        // Remove the root view in case its still showing
                        previousRootViewController?.view.removeFromSuperview()
                    }
                    
                }else{
                    
                     HelperClass.showPopupAlertController(sender: self, message: result.value(forKey: "message") as! String, title: kAlertTitle)
                }
                    
            }else if error != nil {
                HelperClass.showPopupAlertController(sender: self, message: (error?.localizedDescription)!, title: kAlertTitle);
                
            }else {
                HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss();
        }
    }
}




//    func forgotPasswordApi(){
//        SVProgressHUD.show()
//        let urlString = Url_ForgotPassword
//        let perameter: [String: Any] = ["mobileNo": self.txtMobile.text!]
//
//        Alamofire.request(urlString, method: .post, parameters: perameter,encoding: JSONEncoding.default, headers: nil).responseJSON {
//            response in
//            print(response.request!)
//            switch response.result {
//            case .success:
//                print(response.result.value!)
//
//                let JSON = response.result.value! as! NSDictionary
//
//                let errorCode = JSON["errorCode"] as! Int
//                if errorCode == 0{
//                    let customerID = JSON["customerId"] as? NSString
//                    UserDefaults.standard.set(customerID, forKey: kCustomerID)
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPasswordVC") as! ForgotPasswordVC
//                    vc.strCustomerID = customerID! as String
//                    self.navigationController?.pushViewController(vc, animated: true)
//
//                }else{
//                    let message = JSON["message"] as! String
//                    self.addAlertBox(str: message)
//                }
//
//             case .failure(let error):
//                self.addAlertBox(str: "Something went wrong.")
//                print(error)
//            }
//            SVProgressHUD.dismiss()
//        }
//    }
    
//    func loginApi(){
//        SVProgressHUD.show()
//        let urlString = Url_Login
//        let perameter: [String: Any] = ["mobileNo": self.txtMobile.text!,"password": self.txtPassword.text!]
//
//        Alamofire.request(urlString, method: .post, parameters: perameter,encoding: JSONEncoding.default, headers: nil).responseJSON {
//            response in
//            print(response.request!)
//            switch response.result {
//            case .success:
//                print(response.result.value!)
//
//                let JSON = response.result.value! as! NSDictionary
//
//                let errorCode = JSON["errorCode"] as! Int
//                if errorCode == 0{
//                    let dictCustomerInfo = JSON["customerInfo"] as! NSDictionary
//                  //  let dictCustomerInfo = arrCustomerInfo[0] as! NSDictionary
//                    UserDefaults.standard.set(dictCustomerInfo, forKey: kCustomerInfo)
//
//                    let strCustomerID = dictCustomerInfo["customerId"] as! String
//                    UserDefaults.standard.set(strCustomerID, forKey: kCustomerID)
//
//
//                    let userData = UserDHolder.sharedInstance
//                    userData.setValuesForKeys(dictCustomerInfo as! [String : Any])
//
//                    let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
//                    let vc = storyboard.instantiateViewController(withIdentifier: "DashboardNavigationVC") as? UINavigationController
//                    let appDl = UIApplication.shared.delegate as? AppDelegate
//                    let previousRootViewController: UIViewController? = appDl?.window?.rootViewController
//                    appDl?.window?.rootViewController = vc
//
//                    // Allow the view controller to be deallocated
//                    previousRootViewController?.dismiss(animated: false) {
//                        // Remove the root view in case its still showing
//                        previousRootViewController?.view.removeFromSuperview()
//                    }
//
//                }else{
//                    let message = JSON["message"] as! String
//                    self.addAlertBox(str: message)
//                }
//
//            case .failure(let error):
//                self.addAlertBox(str: "Something went wrong.")
//                print(error)
//            }
//           SVProgressHUD.dismiss()
//        }
//    }
