//
//  ForgotPasswordVC.swift
//  DemoMap05Nov19
//
//  Created by Rakesh Gupta on 11/15/19.
//  Copyright © 2019 Rakesh Gupta. All rights reserved.
//

import UIKit
import KWVerificationCodeView
import SVProgressHUD

class ForgotPasswordVC: BaseViewController,KWVerificationCodeViewDelegate{
   
    @IBOutlet weak var viewOTPPassword: KWVerificationCodeView!
    
    @IBOutlet weak var btnVerify: UIButton!
    
    var strCustomerID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HelperClass.addShadowToBtn(btn: btnVerify, radius: 2)
        viewOTPPassword.keyboardType = UIKeyboardType.numberPad
      //  viewOTPPassword.textFont = "1234"
//        if strCustomerID != nil{
//            viewOTPPassword.textFont = "\(strCustomerID)"
//        }
       
    }
    
    func didChangeVerificationCode() {
        viewOTPPassword.hasValidCode()

    }
    
    
    //MARK:- Buttons Action
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func btnVerifyAction(_ sender: Any) {
        viewOTPPassword.focus()
        print(viewOTPPassword.getVerificationCode())
        print(viewOTPPassword.getVerificationCode().count)
       
        if viewOTPPassword.hasValidCode(){
             self.verifyOTPApi()

        }else if viewOTPPassword.getVerificationCode() == "" || viewOTPPassword.getVerificationCode().count == 1{
            self.addAlertBox(str: "Please Enter The OTP")
        }else{
           self.addAlertBox(str: "Please Enter Valid OTP")
        }
        
        viewOTPPassword.clear()
    }
    
    @IBAction func btnResendOTPAction(_ sender: Any) {
         self.resendOTPApi()
    }
    
    //MARK:- Api's functions

    func verifyOTPApi(){
        SVProgressHUD.show()
        let perameter: [String: Any] = ["customerId": strCustomerID,"otp":viewOTPPassword.getVerificationCode()]
        HelperClass.requestForAllApiyWithBody(param: perameter, serverUrl: Url_VerifyOTP, andHeader: true){ (result:NSDictionary , error:Error? , success:Bool ) in
            if success {
                if (result.value(forKey: "errorCode")as! NSInteger == 0){
                    
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPassword") as! ResetPassword
                    self.navigationController?.pushViewController(vc, animated: true)
                   
                }else{
                    
                     HelperClass.showPopupAlertController(sender: self, message: result.value(forKey: "message") as! String, title: kAlertTitle)
                }
                    
            }else if error != nil {
                HelperClass.showPopupAlertController(sender: self, message: (error?.localizedDescription)!, title: kAlertTitle);
                
            }else {
                HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss();
        }
           
    }

    func resendOTPApi(){
        SVProgressHUD.show()
        let perameter: [String: Any] = ["customerId": strCustomerID]
          
        HelperClass.requestForAllApiyWithBody(param: perameter, serverUrl: Url_ResendOTP, andHeader: true) { (result:NSDictionary , error:Error? , success:Bool ) in
            if success {
                if (result.value(forKey: "errorCode")as! NSInteger == 0){
                    
                 HelperClass.showPopupAlertController(sender: self, message: result.value(forKey: "message") as! String, title: kAlertTitle)
                   
                }else{
                    
                     HelperClass.showPopupAlertController(sender: self, message: result.value(forKey: "message") as! String, title: kAlertTitle)
                }
                    
            }else if error != nil {
                HelperClass.showPopupAlertController(sender: self, message: (error?.localizedDescription)!, title: kAlertTitle);
                
            }else {
                HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss();
        }
    }
}





//    func verifyOTPApi(){
//        SVProgressHUD.show()
//        let urlString = Url_VerifyOTP
//        let perameter: [String: Any] = ["customerId": strCustomerID,"otp":viewOTPPassword.getVerificationCode()]
//
//        Alamofire.request(urlString, method: .post, parameters: perameter,encoding: JSONEncoding.default, headers: nil).responseJSON {
//            response in
//            print(response.request!)
//            switch response.result {
//            case .success:
//                print(response.result.value!)
//                let dict = response.result.value! as! NSDictionary
//                let errorCode = dict["errorCode"] as! Int
//                if errorCode == 0{
//                    // let message = JSON["message"] as! String
//                 //   self.addAlertBox(str: message)
//
//                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPassword") as! ResetPassword
//                    self.navigationController?.pushViewController(vc, animated: true)
//
//                }else{
//                    let message = dict["message"] as! String
//                    self.addAlertBox(str: message)
//                }
//
//            case .failure(let error):
//                self.addAlertBox(str: "Something went wrong.")
//                print(error)
//            }
//            SVProgressHUD.dismiss()
//        }
//    }
    
//    func resendOTPApi(){
//        SVProgressHUD.show()
//        let urlString = Url_ResendOTP
//        let perameter: [String: Any] = ["customerId": strCustomerID]
//
//        Alamofire.request(urlString, method: .post, parameters: perameter,encoding: JSONEncoding.default, headers: nil).responseJSON {
//            response in
//            print(response.request!)
//            switch response.result {
//            case .success:
//                print(response.result.value!)
//                self.addAlertBox(str: "OTP regenerated successfull.")
//
//            case .failure(let error):
//                self.addAlertBox(str: "Something went wrong.")
//                print(error)
//            }
//            SVProgressHUD.dismiss()
//        }
//    }
//
