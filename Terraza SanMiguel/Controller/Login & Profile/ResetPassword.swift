//
//  ResetPassword.swift
//  DemoMap05Nov19
//
//  Created by Rakesh Gupta on 11/18/19.
//  Copyright © 2019 Rakesh Gupta. All rights reserved.
//

import UIKit
import ACFloatingTextfield_Swift
import SVProgressHUD

class ResetPassword: BaseViewController {

    @IBOutlet weak var txtNewPassword: ACFloatingTextfield!
    @IBOutlet weak var txtConfirmPassword: ACFloatingTextfield!
    
    @IBOutlet weak var btn1Eye: UIButton!
    @IBOutlet weak var btn2Eye: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    
    
    @IBOutlet weak var viewNewPassword: UIView!
    @IBOutlet weak var viewConfirmPassword: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HelperClass.addShadowToBtn(btn: btnSubmit, radius: 2)
        viewNewPassword.layer.borderWidth = 0.5
        viewConfirmPassword.layer.borderWidth = 0.5
        viewNewPassword.layer.borderColor = UIColor.lightGray.cgColor
        viewConfirmPassword.layer.borderColor = UIColor.lightGray.cgColor
      
    }
    
    //MARK:- Buttons Action
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func btnShowHidePassAction(_ sender: UIButton) {
        
        if sender.tag == 0{
            if txtNewPassword.isSecureTextEntry == true{
                txtNewPassword.isSecureTextEntry = false
                self.btn1Eye.setImage(UIImage.init(named: "eye.png"), for: .normal)
            }else{
                txtNewPassword.isSecureTextEntry = true
                self.btn1Eye.setImage(UIImage.init(named: "1.png"), for: .normal)
            }
        }else{
            if txtConfirmPassword.isSecureTextEntry == true{
                txtConfirmPassword.isSecureTextEntry = false
                self.btn2Eye.setImage(UIImage.init(named: "eye.png"), for: .normal)
            }else{
                txtConfirmPassword.isSecureTextEntry = true
                self.btn2Eye.setImage(UIImage.init(named: "1.png"), for: .normal)
            }

        }
    }
    
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        if txtNewPassword.text!.isEmpty{
            self.addAlertBox(str: "Please Enter New Password")
        }else if txtConfirmPassword.text!.isEmpty {
            self.addAlertBox(str: "Please Confirm Password")
        }else if txtNewPassword.text != txtConfirmPassword.text{
            self.addAlertBox(str: "Mismatch The Password")
        }else{
            self.resetPasswordApi()
        }
    }
    
    
    //MARK:- Api's Functions
    
    func resetPasswordApi(){
        SVProgressHUD.show()
        let strCustomerID = UserDefaults.standard.string(forKey: kCustomerID)
        let perameter: [String: Any] = ["customerId": strCustomerID!,"password": self.txtConfirmPassword.text!]
        
        HelperClass.requestForAllApiyWithBody(param: perameter, serverUrl: Url_ChangePassword, andHeader: true){ (result:NSDictionary , error:Error? , success:Bool ) in
            if success {
                if (result.value(forKey: "errorCode")as! NSInteger == 0){
                    let message = result["message"] as! String
                    self.addAlertBox(str: message)
                    
                    let dictCustomerInfo = result["customerInfo"] as! NSDictionary
                    //   let dictCustomerInfo = arrCustomerInfo[0] as! NSDictionary
                    UserDefaults.standard.set(dictCustomerInfo, forKey: kCustomerInfo)
                    
                    let strCustomerID = dictCustomerInfo["customerId"] as! String
                    UserDefaults.standard.set(strCustomerID, forKey: kCustomerID)
                    
                    let userData = UserDHolder.sharedInstance
                    userData.setValuesForKeys(dictCustomerInfo as! [String : Any])
                    
                    let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
                    let vc = storyboard.instantiateViewController(withIdentifier: "DashboardNavigationVC") as? UINavigationController
                    let appDl = UIApplication.shared.delegate as? AppDelegate
                    let previousRootViewController: UIViewController? = appDl?.window?.rootViewController
                    appDl?.window?.rootViewController = vc
                    
                    // Allow the view controller to be deallocated
                    previousRootViewController?.dismiss(animated: false) {
                        // Remove the root view in case its still showing
                        previousRootViewController?.view.removeFromSuperview()
                    }
                }else{
                    
                    HelperClass.showPopupAlertController(sender: self, message: result.value(forKey: "message") as! String, title: kAlertTitle)
                }
                
            }else if error != nil {
                HelperClass.showPopupAlertController(sender: self, message: (error?.localizedDescription)!, title: kAlertTitle);
                
            }else {
                HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss();
        }
    }

}






//    func resetPasswordApi(){
//        SVProgressHUD.show()
//        print(self.userDetails.customerId)
//        print(self.txtConfirmPassword.text!)
//        let urlString = Url_ChangePassword
//        let strCustomerID = UserDefaults.standard.string(forKey: kCustomerID)
//
//        let perameter: [String: Any] = ["customerId": strCustomerID!,"password": self.txtConfirmPassword.text!]
//
//        Alamofire.request(urlString, method: .post, parameters: perameter,encoding: JSONEncoding.default, headers: nil).responseJSON {
//            response in
//            print(response.request!)
//            switch response.result {
//            case .success:
//                print(response.result.value!)
//
//                let dict = response.result.value! as! NSDictionary
//
//                let errorCode = dict["errorCode"] as! Int
//                if errorCode == 0{
//                    let message = dict["message"] as! String
//                    self.addAlertBox(str: message)
//
//                    let dictCustomerInfo = dict["customerInfo"] as! NSDictionary
//                 //   let dictCustomerInfo = arrCustomerInfo[0] as! NSDictionary
//                     UserDefaults.standard.set(dictCustomerInfo, forKey: kCustomerInfo)
//
//                    let strCustomerID = dictCustomerInfo["customerId"] as! String
//                    UserDefaults.standard.set(strCustomerID, forKey: kCustomerID)
//
//                    let userData = UserDHolder.sharedInstance
//                    userData.setValuesForKeys(dictCustomerInfo as! [String : Any])
//
//                    let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
//                    let vc = storyboard.instantiateViewController(withIdentifier: "DashboardNavigationVC") as? UINavigationController
//                    let appDl = UIApplication.shared.delegate as? AppDelegate
//                    let previousRootViewController: UIViewController? = appDl?.window?.rootViewController
//                    appDl?.window?.rootViewController = vc
//
//                    // Allow the view controller to be deallocated
//                    previousRootViewController?.dismiss(animated: false) {
//                        // Remove the root view in case its still showing
//                        previousRootViewController?.view.removeFromSuperview()
//                    }
//                }else{
//                    let message = dict["message"] as! String
//                    self.addAlertBox(str: message)
//                }
//
//            case .failure(let error):
//                self.addAlertBox(str: "Something went wrong.")
//                print(error)
//            }
//            SVProgressHUD.dismiss()
//        }
//    }
