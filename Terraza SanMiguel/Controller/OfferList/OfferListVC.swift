//
//  OfferListVC.swift
//  Terraza SanMiguel
//
//  Created by Rakesh Gupta on 11/26/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import UIKit
import SVProgressHUD

class OfferListVC: BaseViewController,UITableViewDataSource,UITableViewDelegate{
 
    @IBOutlet weak var tblOfferList: UITableView!
    var arrOfferList = [OfferListData]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if UserDefaults.standard.string(forKey: "SelectedLanguage") == "en"{
                  self.setTitleOnNavigationBar(name: "Offers")
              }else{
                  self.setTitleOnNavigationBar(name: "Pedidos")
              }
        
         getOfferListApi()
    
    }
    
    //MARK:- Tableview Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrOfferList.count
     }
     
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tblOfferList.dequeueReusableCell(withIdentifier: "OfferListCell") as! OfferListCell
        let offerData = arrOfferList[indexPath.row]
        cell.lblOfferDesc.text = offerData.couponDescription
        cell.lblTitleOffer.text = offerData.couponTitle
        cell.lblInnerOfferView.text = offerData.couponCode
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tblOfferList.deselectRow(at: indexPath, animated: true)
    }

    
    //MARK:- Api functions
    func getOfferListApi(){
        SVProgressHUD.show()
        let strCustomerID = UserDefaults.standard.string(forKey: kCustomerID)
        let perameter: [String: Any] = ["customerId": strCustomerID!]
        HelperClass.requestForAllApiyWithBody(param: perameter, serverUrl: Url_OfferList, andHeader: true){ (result:NSDictionary , error:Error? , success:Bool ) in
            if success {
                if (result.value(forKey: "errorCode")as! NSInteger == 0){
                    
                    self.arrOfferList.removeAll()
                    if let arrTemp = result["offerList"] as? NSArray {
                                                         
                        for dict in arrTemp {
                            let data = OfferListData.init(data: dict as! Dictionary<String, Any>)
                            self.arrOfferList.append(data)
                        }
                    }
                    self.tblOfferList.reloadData()
                   
                }else{
                    
                     HelperClass.showPopupAlertController(sender: self, message: result.value(forKey: "message") as! String, title: kAlertTitle)
                }
                    
            }else if error != nil {
                HelperClass.showPopupAlertController(sender: self, message: (error?.localizedDescription)!, title: kAlertTitle);
                
            }else {
                HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss();
        }
    }
    
}





//    func getOfferListApi(){
//        SVProgressHUD.show()
//        let urlString = Url_OfferList
//        let strCustomerID = UserDefaults.standard.string(forKey: kCustomerID)
//        let perameter: [String: Any] = ["customerId": strCustomerID!]
//
//        Alamofire.request(urlString, method: .post, parameters: perameter,encoding: JSONEncoding.default, headers: nil).responseJSON {
//            response in
//            print(response.request!)
//            switch response.result {
//            case .success:
//                print(response.result.value!)
//
//                let dict = response.result.value! as! NSDictionary
//
//                let errorCode = dict["errorCode"] as! Int
//                if errorCode == 0{
//
//                    self.arrOfferList.removeAll()
//                    if let arrTemp = dict["offerList"] as? NSArray {
//
//                        for dict in arrTemp {
//                            let data = OfferListData.init(data: dict as! Dictionary<String, Any>)
//                            self.arrOfferList.append(data)
//                        }
//                    }
//                    self.tblOfferList.reloadData()
//
//
//                }else{
//                    let message = dict["message"] as! String
//                    self.addAlertBox(str: message)
//                }
//
//            case .failure(let error):
//                self.addAlertBox(str: "Something went wrong.")
//                print(error)
//            }
//           SVProgressHUD.dismiss()
//        }
//    }
