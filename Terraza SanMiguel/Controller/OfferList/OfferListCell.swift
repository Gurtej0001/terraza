//
//  OfferListCell.swift
//  Terraza SanMiguel
//
//  Created by Rakesh Gupta on 11/26/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import UIKit

class OfferListCell: UITableViewCell {

    @IBOutlet weak var viewOfferMain: UIView!
    @IBOutlet weak var lblInnerOfferView: UILabel!
    @IBOutlet weak var lblTitleOffer: UILabel!
    @IBOutlet weak var lblOfferDesc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewOfferMain.layer.borderWidth = 1
        viewOfferMain.layer.borderColor = themeColorDarkGreen.cgColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
