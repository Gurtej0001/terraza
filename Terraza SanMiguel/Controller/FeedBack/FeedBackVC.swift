//
//  FeedBackVC.swift
//  Terraza SanMiguel
//
//  Created by Rakesh Gupta on 11/26/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import UIKit
import Cosmos
import SVProgressHUD

class FeedBackVC: BaseViewController,UITextViewDelegate{

    
    @IBOutlet weak var txtViewFeedback: UITextView!
    
    @IBOutlet weak var viewFoodRating: CosmosView!
    @IBOutlet weak var viewServiceRating: CosmosView!
    @IBOutlet weak var btnSubmit: UIButton!
    
    var strOrderID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        txtViewFeedback.layer.borderColor = UIColor.lightGray.cgColor
        txtViewFeedback.layer.borderWidth = 1
        btnSubmit.layer.borderColor = themeColorLightGreen.cgColor
        btnSubmit.layer.borderWidth = 1
        HelperClass.addShadowToBtn(btn: btnSubmit, radius: 2)
        
        self.setTitleOnNavigationBar(name: "Order #" + strOrderID)
    }
    
    
    //MARK:- TextView Delegats
    func textViewDidBeginEditing(_ textView: UITextView) {
     //   textView.backgroundColor = UIColor.lightGray
        if textView.text == "Enter Feedback"{
            txtViewFeedback.text = ""
            txtViewFeedback.textColor = .black
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
           if text == "\n" {
               textView.resignFirstResponder()
               return false
           }
        
           return true
    }
    
    
    func textViewDidEndEditing(_ textView: UITextView) {
        textView.backgroundColor = UIColor.white
       
        if textView.text == ""{
            txtViewFeedback.text = "Enter Feedback"
            txtViewFeedback.textColor = .lightGray
        }
    }
    
    //MARK:- Buttons Action
    @IBAction func btnSubmitAction(_ sender: Any) {
        if txtViewFeedback.text == "Enter Feedback"{
               txtViewFeedback.text = ""
        }
        
        if txtViewFeedback.text == ""{
            self.addAlertBox(str: "please enter the text")
            txtViewFeedback.text = "Enter Feedback"
            txtViewFeedback.textColor = .lightGray
        }else{
           self.feedbackApi()
        }
        
    }
    
    //MARK:- Api's Function
    
    func feedbackApi() {
            
        SVProgressHUD.show()
           
        let perameter: [String: Any] = ["customerName": self.userDetails.customerName ,"email": self.userDetails.email,"feedback":self.txtViewFeedback.text!,"foodRating":self.viewFoodRating.rating,"serviceRating":self.viewServiceRating.rating,"orderId":strOrderID]
            
        HelperClass.requestForAllApiyWithBody(param: perameter , serverUrl: Url_FeedBack as String, andHeader: false) { (result:NSDictionary , error:Error? , success:Bool ) in
                if success {
                    if (result.value(forKey: "errorCode")as! NSInteger == 0){
                                      
                        let message = result["message"] as! String
                        self.navigationController?.popViewController(animated: true)
                       // self.addAlertBox(str: message)
                         HelperClass.showPopupAlertController(sender: self, message: message, title: kAlertTitle);
                                          
                    }else{
                        let message = result["message"] as! String
                       // self.addAlertBox(str: message)
                         HelperClass.showPopupAlertController(sender: self, message: message, title: kAlertTitle);
                    }
                   
                }else if error != nil {
                     HelperClass.showPopupAlertController(sender: self, message: (error?.localizedDescription)!, title: kAlertTitle);
                }else {
                     HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
                }
            SVProgressHUD.dismiss()
        }
            
    }
}
