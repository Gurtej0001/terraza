//
//  BaseViewController.swift
//  Terraza SanMiguel
//
//  Created by Rakesh gupta on 11/13/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import UIKit
import Foundation
import SVProgressHUD
import ACFloatingTextfield_Swift

class BaseViewController: UIViewController {
    
    var viewMenu : SideMenuView!
    let userDetails: UserDHolder! = UserDHolder.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        let statusBarHeight = CGFloat(40)
        let colorView = UIView(frame: CGRect(x: 0, y: -statusBarHeight, width: self.view.bounds.width, height: statusBarHeight))
        colorView.backgroundColor = themeColorLightGreen
        self.navigationController?.navigationBar.addSubview(colorView)
        

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
           return .lightContent
    }
    
    
    func setTitleOnNavigationBar(name : String) -> Void {
      
        self.navigationController?.isNavigationBarHidden = false
        let font: UIFont? = UIFont.init(name: "quarca", size: CGFloat(22))!//UIFont.boldSystemFont(ofSize: CGFloat(fontSize))
        let attributes = [NSAttributedString.Key.font: font]
        
        let btnItemBack = UIBarButtonItem(image: UIImage(named: "back-icon"), style: .plain, target: self, action: #selector(self.backButtonAction))
             btnItemBack.tintColor = UIColor.white
       
          //   btnItemBack.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        btnItemBack.setTitleTextAttributes(attributes as [NSAttributedString.Key : Any], for: .normal)
             
        let btnItemText = UIBarButtonItem(title: name, style: .plain, target: self, action: nil)
        btnItemText.tintColor = UIColor.white
             
        btnItemText.setTitleTextAttributes(attributes as [NSAttributedString.Key : Any], for: .normal)
             let items1 = [btnItemBack, btnItemText]
        navigationItem.leftBarButtonItems = items1
        
     
        self.navigationController?.navigationBar.barStyle = .default
      
        self.navigationController!.navigationBar.tintColor = UIColor.white
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil);
       
    }
    
    
    
    func addAlertBox(str: String){
        let alert = UIAlertController(title: "", message: str, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func backButtonAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnMenuAction(_ sender: Any) {
        let arrViews: Array? = Bundle.main.loadNibNamed("SideMenu", owner: self, options: nil)
        viewMenu = (arrViews?[0] as? SideMenuView)!
        
        viewMenu.frame = CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height)
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(tapGesture(gesture:)));
        viewMenu.viewBackground.addGestureRecognizer(tapGesture)
       // subscriptioView.isDetailPage = detailPage
        //isSubscriptionView = true
        self.viewMenu.sizeToFit()
        self.viewMenu.clipsToBounds = true
        self.viewMenu.layoutIfNeeded()
        UIApplication.shared.keyWindow?.addSubview(viewMenu)
        
    }
    
    @objc func tapGesture(gesture : UITapGestureRecognizer){
        viewMenu.viewBackground.removeGestureRecognizer(gesture)
        viewMenu.removeFromSuperview();
        //isSubscriptionView = false
        
    }
}

extension ACFloatingTextfield {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}


extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension Int{
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
}
