//
//  SideMenuView.swift
//  Terraza SanMiguel
//
//  Created by Rakesh gupta on 11/14/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import UIKit
import SVProgressHUD

class SideMenuView: UIView {
    
    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var viewProfile: UIView!
 
  
    override func draw(_ rect: CGRect) {
        // Drawing code
        HelperClass.addShadowToView(view: self.viewProfile, radius: 2.0)
      
    }
 
    //MARK: - Button Action

    @IBAction func btnProfileAction(_ sender: Any)   {
        self.removeFromSuperview()
        let vcSideMenu = (UIApplication.topViewController()?.storyboard!.instantiateViewController(withIdentifier: "ProfileViewController"))!
        UIApplication.topViewController()?.navigationController!.pushViewController(vcSideMenu, animated: true)
    }
    
    @IBAction func btnLanguageAction(_ sender: Any) {
                
        let arrViews: Array? = Bundle.main.loadNibNamed("ViewSelectLanguage", owner: self, options: nil)
        let viewLanguagePopup = (arrViews?[0] as? ViewSelectLanguage)!
        
        viewLanguagePopup.frame = UIApplication.shared.keyWindow!.frame
      //  self.addSubview(viewLanguagePopup)
         UIApplication.shared.keyWindow!.addSubview(viewLanguagePopup)
        
     }
    
    @IBAction func btnLogoutAction(_ sender: Any) {
        
        let viewLogoutPopUp = (UINib(nibName: "AlertPopUp", bundle: nil).instantiate(withOwner: self, options: nil)[0] as? AlertPopUp)!
        viewLogoutPopUp.frame = UIApplication.shared.keyWindow!.frame
       
     //   viewLogoutPopUp.btnYes.addTarget(self, action: #selector(btnYesLogoutAction), for: .touchUpInside)
        UIApplication.shared.keyWindow!.addSubview(viewLogoutPopUp)
        
    }
    
  
}



