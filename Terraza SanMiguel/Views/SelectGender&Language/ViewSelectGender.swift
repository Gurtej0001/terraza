//
//  ViewSelectGender.swift
//  DemoMap05Nov19
//
//  Created by Rakesh Gupta on 11/18/19.
//  Copyright © 2019 Rakesh Gupta. All rights reserved.
//

import UIKit

protocol SenderViewControllerDelegate {
    func strMessage(str: String)
}

class ViewSelectGender: UIView,SenderViewControllerDelegate {
   
    @IBOutlet weak var btnMale: UIButton!
    @IBOutlet weak var btnFemale: UIButton!
    var strGenderType = String()
    
   
    @IBOutlet weak var viewMain: UIView!
    
    override func awakeFromNib() {
       // viewMain.layer.borderWidth = 0.5
      //  viewMain.layer.borderColor = UIColor.lightGray.cgColor
        HelperClass.addShadowToView(view: viewMain, radius: 5)
    }
    
    func strMessage(str: String) {
        print(str)
        strGenderType = str
        if strGenderType == "Male" || strGenderType == "Masculina"{
            btnMale.setBackgroundImage(UIImage.init(named: "checked.png"), for: .normal)
            btnFemale.setBackgroundImage(UIImage.init(named: "unchecked.png"), for: .normal)
        }else{
            btnFemale.setBackgroundImage(UIImage.init(named: "checked.png"), for: .normal)
            btnMale.setBackgroundImage(UIImage.init(named: "unchecked.png"), for: .normal)
        }
    }
    
    @IBAction func btnRadioAction(_ sender: UIButton) {
       
        if sender.tag == 0 {
            btnMale.setBackgroundImage(UIImage.init(named: "checked.png"), for: .normal)
            btnFemale.setBackgroundImage(UIImage.init(named: "unchecked.png"), for: .normal)
            
            if UserDefaults.standard.string(forKey: "SelectedLanguage") == "en"{
                strGenderType = "Male"
            }else{
                strGenderType = "Masculina"
            }
             
        }else{
            btnFemale.setBackgroundImage(UIImage.init(named: "checked.png"), for: .normal)
            btnMale.setBackgroundImage(UIImage.init(named: "unchecked.png"), for: .normal)
          
             if UserDefaults.standard.string(forKey: "SelectedLanguage") == "en"{
                strGenderType = "Female"
            }else{
                strGenderType = "Hembra"
            }
        }
     
    }
    
    
    @IBAction func btnDoneAction(_ sender: UIButton) {
        
        NotificationCenter.default.post(name: Notification.Name("strGenderType"), object: strGenderType)
        self.removeFromSuperview()
    }
    
    @IBAction func btnClossAction(_ sender: UIButton) {
         self.removeFromSuperview()
    }
    
    
    
}
