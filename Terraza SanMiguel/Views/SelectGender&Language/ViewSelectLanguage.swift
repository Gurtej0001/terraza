//
//  ViewSelectLanguage.swift
//  DemoMap05Nov19
//
//  Created by Rakesh Gupta on 11/19/19.
//  Copyright © 2019 Rakesh Gupta. All rights reserved.
//

import UIKit
import LanguageManager_iOS

class ViewSelectLanguage: UIView {

 
    @IBOutlet weak var btnEnglish: UIButton!
    @IBOutlet weak var btnSpenish: UIButton!
    
    var selectedLanguage: Languages = .es
    var strLanguage = String()
    
    override func awakeFromNib() {
        
        if UserDefaults.standard.string(forKey: "SelectedLanguage") == "en"{
            
            btnEnglish.setBackgroundImage(UIImage.init(named: "checked.png"), for: .normal)
            btnSpenish.setBackgroundImage(UIImage.init(named: "unchecked.png"), for: .normal)
        }else{
           btnSpenish.setBackgroundImage(UIImage.init(named: "checked.png"), for: .normal)
           btnEnglish.setBackgroundImage(UIImage.init(named: "unchecked.png"), for: .normal)
        }
    }
    
    //MARK:- Buttons's Action
    
    @IBAction func btnRadioAction(_ sender: UIButton) {
        
        if sender.tag == 0{
            btnEnglish.setBackgroundImage(UIImage.init(named: "checked.png"), for: .normal)
            btnSpenish.setBackgroundImage(UIImage.init(named: "unchecked.png"), for: .normal)
            self.selectedLanguage = .en
            strLanguage = "en"
        }else{
            btnSpenish.setBackgroundImage(UIImage.init(named: "checked.png"), for: .normal)
            btnEnglish.setBackgroundImage(UIImage.init(named: "unchecked.png"), for: .normal)
            self.selectedLanguage = .es
            strLanguage = "es"
        }
       
        
    //    let storyboard = UIStoryboard(name: "Main", bundle: nil)
        // the view controller that you want to show after changing the language
    //    let viewController = storyboard.instantiateInitialViewController()
        
        // change the language
     
    }
    
    @IBAction func btnDoneAction(_ sender: UIButton) {
      self.removeFromSuperview()
        
        print(self.selectedLanguage)
        
        UserDefaults.standard.set(strLanguage, forKey: "SelectedLanguage")
           //  LanguageManager.shared.currentLanguage = self.selectedLanguage
             let vc = (UIApplication.topViewController()?.storyboard!.instantiateViewController(withIdentifier: "DashboardNavigationVC"))!
             
             LanguageManager.shared.setLanguage(language: selectedLanguage, rootViewController: vc, animation: { view in
                 // do custom animation
                 view.transform = CGAffineTransform(scaleX: 2, y: 2)
                 view.alpha = 0
             })
    }
    
    @IBAction func btnCloseAction(_ sender: Any) {
        self.removeFromSuperview()
    }
    
}
