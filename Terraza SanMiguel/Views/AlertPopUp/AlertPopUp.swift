//
//  AlertPopUp.swift
//  Jrney
//
//  Created by Rakesh Gupta on 8/14/19.
//  Copyright © 2019 Rakesh gupta. All rights reserved.
//

import UIKit
import SVProgressHUD

class AlertPopUp: UIView {
    
    @IBOutlet weak var viewCenterAlert: UIView!
    @IBOutlet weak var lblTitleAlert: UILabel!
    @IBOutlet weak var btnYes: UIButton!
   
    @IBOutlet weak var viewBtnYesNo: UIView!
   
    
    override func awakeFromNib() {
    HelperClass.addShadowToView(view: viewCenterAlert, radius: 5)
    }
    
    
    @IBAction func btnNoAction(_ sender: Any) {
        
        self.removeFromSuperview()
    }
    
    @IBAction func btnYesAction(_ sender: Any) {
       self.logoutApi()
        
    }
    
    //MARK:- API's Function
    func logoutApi(){
        SVProgressHUD.show()
        let strCustomerID = UserDefaults.standard.string(forKey: kCustomerID)
        let perameter: [String: Any] = ["customerId": strCustomerID!]
           
        HelperClass.requestForAllApiyWithBody(param: perameter, serverUrl: Url_Logout, andHeader: true){ (result:NSDictionary , error:Error? , success:Bool ) in
            if success {
                if (result.value(forKey: "errorCode")as! NSInteger == 0){
                    
                    let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
                    let vc = storyboard.instantiateViewController(withIdentifier: "LoginNavigationVC") as? UINavigationController
                    let appDl = UIApplication.shared.delegate as? AppDelegate
                    let previousRootViewController: UIViewController? = appDl?.window?.rootViewController
                    appDl?.window?.rootViewController = vc
                    
                    // Allow the view controller to be deallocated
                    previousRootViewController?.dismiss(animated: false) {
                        // Remove the root view in case its still showing
                        previousRootViewController?.view.removeFromSuperview()
                    }
                    UserDHolder.destroy()
                    UserDefaults.standard.removeObject(forKey: kCustomerInfo)
                    UserDefaults.standard.removeObject(forKey: kCustomerID)
                    
                }else{
                    
                     HelperClass.showPopupAlertController(sender: self, message: result.value(forKey: "message") as! String, title: kAlertTitle)
                }
                    
            }else if error != nil {
                HelperClass.showPopupAlertController(sender: self, message: (error?.localizedDescription)!, title: kAlertTitle);
                
            }else {
                HelperClass.showPopupAlertController(sender: self, message: "Request failed,Please try again", title: kAlertTitle);
            }
            SVProgressHUD.dismiss();
        }
    }
    
}




//    func logoutApi(){
//        SVProgressHUD.show()
//        let urlString = Url_Logout
//        let strCustomerID = UserDefaults.standard.string(forKey: kCustomerID)
//        let perameter: [String: Any] = ["customerId": strCustomerID!]
//
//        Alamofire.request(urlString, method: .post, parameters: perameter,encoding: JSONEncoding.default, headers: nil).responseJSON {
//            response in
//            print(response.request!)
//            switch response.result {
//            case .success:
//                print(response.result.value!)
//                self.removeFromSuperview()
//                let JSON = response.result.value! as! NSDictionary
//
//                let errorCode = JSON["errorCode"] as! Int
//                if errorCode == 0{
//
//                    let storyboard = UIStoryboard.init(name: "Main", bundle: Bundle.main)
//                    let vc = storyboard.instantiateViewController(withIdentifier: "LoginNavigationVC") as? UINavigationController
//                    let appDl = UIApplication.shared.delegate as? AppDelegate
//                    let previousRootViewController: UIViewController? = appDl?.window?.rootViewController
//                    appDl?.window?.rootViewController = vc
//
//                    // Allow the view controller to be deallocated
//                    previousRootViewController?.dismiss(animated: false) {
//                        // Remove the root view in case its still showing
//                        previousRootViewController?.view.removeFromSuperview()
//                    }
//                    UserDHolder.destroy()
//                    UserDefaults.standard.removeObject(forKey: kCustomerInfo)
//                    UserDefaults.standard.removeObject(forKey: kCustomerID)
//
//                }else{
//                    //let message = JSON["message"] as! String
//                    // self.addAlertBox(str: message)
//                }
//
//            case .failure(let error):
//
//                print(error)
//            }
//            SVProgressHUD.dismiss()
//        }
//    }
